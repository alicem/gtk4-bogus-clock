public class ClockDemo.Arrow : Gtk.Widget {
    public Arrow (string name) {
        Object (css_name: name);
    }
}

public class ClockDemo.Clock : Gtk.Widget {
    private int age = 0;

    static construct {
        set_css_name ("clock");
    }

    construct {
        new Arrow ("arrow1").set_parent (this);
        new Arrow ("arrow2").set_parent (this);
        new Arrow ("arrow3").set_parent (this);

        add_tick_callback (() => {
            if (!get_mapped ())
                return Source.REMOVE;

            age++;
            queue_allocate ();

            return Source.CONTINUE;
        });
    }

    protected override void size_allocate (int width, int height, int baseline) {
        int i = 1;

        for (var arrow = get_first_child (); arrow != null; arrow = arrow.get_next_sibling ()) {
            int min;
            arrow.measure (Gtk.Orientation.VERTICAL, -1, out min, null, null, null);

            var t = new Gsk.Transform ();
            t = t.translate ({ width / 2, height / 2 });
            t = t.rotate ((float) age / (float) i++);

            arrow.allocate (1, min, baseline, t);
        }
    }

    protected override void dispose () {
        var child = get_first_child ();
        while (child != null) {
            var c = child;
            child = child.get_next_sibling ();

            c.unparent ();
        }

        base.dispose ();
    }
}
